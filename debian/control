Source: biomaj3-user
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Olivier Sallou <osallou@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-bcrypt,
               python3-consul,
               python3-flask,
               python3-ldap3,
               python3-mock,
               python3-pymongo,
               python3-setuptools,
               python3-tabulate,
               python3-yaml,
               python3-biomaj3-core
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/biomaj3-user
Vcs-Git: https://salsa.debian.org/med-team/biomaj3-user.git
Homepage: https://github.com/genouest/biomaj-user
Rules-Requires-Root: no

Package: python3-biomaj3-user
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Recommends: ${python3:Recommends}
Suggests: ${python3:Suggests},
          python3-gunicorn
Description: BioMAJ user management library
 BioMAJ downloads remote data banks, checks their status and applies
 transformation workflows, with consistent state, to provide ready-to-use
 data for biologists and bioinformaticians. For example, it can transform
 original FASTA files into BLAST indexes. It is very flexible and its
 post-processing facilities can be extended very easily.
 .
 BioMAJ3 is a rewrite of BioMAJ v1.x, see online documentation for migration.
 .
 This package contains the library and microservice to manage users in BioMAJ3
XB-Python-Egg-Name: biomaj-user
